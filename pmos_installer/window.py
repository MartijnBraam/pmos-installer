from gi.repository import Gtk

import pmos_installer
from .devices import get_devices


class ListBoxRowWithData(Gtk.ListBoxRow):
    def __init__(self, data):
        super(Gtk.ListBoxRow, self).__init__()
        self.data = data


selected_device = None


@Gtk.Template(resource_path='/org/postmarketos/installer/window.ui')
class PmosInstallerWindow(Gtk.Assistant):
    __gtype_name__ = 'PmosInstallerWindow'

    listbox_devices = Gtk.Template.Child("listbox-devices")

    # All the pages in the wizard
    page_introductionbox = Gtk.Template.Child("introductionbox")
    page_encryptionbox = Gtk.Template.Child("encryptionbox")
    page_userinfobox = Gtk.Template.Child("userinfobox")

    @Gtk.Template.Callback("on_cancel_wizard")
    def on_cancel_wizard(self, *args):
        Gtk.main_quit(*args)

    @Gtk.Template.Callback("on_close_wizard")
    def on_close_wizard(self, *args):
        Gtk.main_quit(*args)

    @Gtk.Template.Callback("on_apply")
    def on_apply(self, button):
        print("Hello World!")

    @Gtk.Template.Callback("on_refresh_devices")
    def on_refresh_devices(self, button):
        print("refreshing devices...")
        # TODO: do this async
        devices = get_devices()

        # Clear current device list
        # for child in listbox_devices.children:
        #    listbox_devices.remove(child)

        # Build new listbox items for the devices
        for device in devices:
            row = ListBoxRowWithData(device)
            hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
            vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
            if device["type"] == "adb":
                pixbuf = Gtk.IconTheme.get_default().load_icon("phone", 64, 0)
                device_name = Gtk.Label(device["name"], xalign=0)
                device_name.set_markup("<b>{}</b>".format(device["name"]))
                vbox.pack_start(device_name, True, True, 0)
                device_detail = Gtk.Label(device["serial"], xalign=0)
                vbox.pack_start(device_detail, True, True, 0)
            elif device["type"] == "sdcard":
                pixbuf = Gtk.IconTheme.get_default().load_icon("drive-removable-media", 64, 0)
                device_name = Gtk.Label(device["name"], xalign=0)
                device_name.set_markup("<b>{}</b>".format(device["name"]))
                vbox.pack_start(device_name, True, True, 0)
                device_detail = Gtk.Label("Size: {}".format(device["size"]), xalign=0)
                vbox.pack_start(device_detail, True, True, 0)
            icon = Gtk.Image.new_from_pixbuf(pixbuf)
            alignment1 = Gtk.Alignment()
            alignment1.add(icon)
            alignment1.set(-1, 0, 0, 0)
            hbox.pack_start(alignment1, True, True, 0)
            alignment2 = Gtk.Alignment()
            alignment2.add(vbox)
            alignment2.set(-1, 0, 0, 0)
            hbox.pack_start(alignment2, True, True, 0)
            row.add(hbox)
            self.listbox_devices.add(row)
        self.listbox_devices.show_all()

    def finish_page(self, page, state=True):
        page = 'page_' + page
        if hasattr(self, page):
            page = getattr(self, page)
        else:
            print("Invalid page '{}'".format(page))

        self.set_page_complete(page, state)

    @Gtk.Template.Callback("on_select_device")
    def on_select_device(self, widget, row):
        global selected_device
        selected_device = row.data
        print("Selected {}.".format(selected_device["name"]))
        self.finish_page("introductionbox")

    @Gtk.Template.Callback("on_fde_password_changed")
    def on_fde_password_changed(self, textbox):
        if len(textbox.get_text()) == 0:
            self.finish_page("encryptionbox", False)
        else:
            self.finish_page("encryptionbox")

    @Gtk.Template.Callback("on_enable_disable_fde")
    def on_enable_disable_fde(self, checkbox):
        fde_password_entry = builder.get_object("fde-password")
        if checkbox.get_active():
            fde_password_entry.set_editable(True)
            if len(fde_password_entry.get_text()) == 0:
                self.finish_page("encryptionbox", False)
            else:
                self.finish_page("encryptionbox")
            print("FDE Enabled")
        else:
            fde_password_entry.set_editable(False)
            print("FDE Disabled")
            self.finish_page("encryptionbox")

    @Gtk.Template.Callback("on_userinfo_changed")
    def on_userinfo_changed(self, textbox):
        boxes = ["username", "password", "hostname"]
        complete = True
        for box in boxes:
            widget = builder.get_object(box)
            if widget.get_text() == "":
                complete = False
                break

        if complete:
            self.finish_page("userinfobox")
        else:
            self.finish_page("userinfobox", True)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
